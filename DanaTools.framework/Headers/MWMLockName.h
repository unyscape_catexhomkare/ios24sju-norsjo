//
//  MWMDeviceName.h
//  Danalock3
//
//  Created by Daniel Amkær Sørensen on 26/01/15.
//  Copyright (c) 2015 Daniel Amkær Sørensen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MWMLockName : NSObject
+ (NSString*) nameWithMacAddress: (NSString*) macAddress;
+ (NSString*) macAddressWithName: (NSString*) name;
@end
