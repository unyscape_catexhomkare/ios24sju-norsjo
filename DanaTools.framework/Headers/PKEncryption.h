//
//  PKEncryption.h
//  Ekeyapp
//
//  Created by Daniel Amkær Sørensen on 20/06/14.
//
//

#import <Foundation/Foundation.h>

@interface PKEncryption : NSObject

@property (nonatomic, copy) NSData* encryptionKey;

- (PKEncryption*) initWithKey: (NSData*)key;
- (PKEncryption*) initWithPLEK: (NSData*)PLEK nonce: (NSData*)nonce;
+ (NSData *) keyWithPLEK: (NSData*)PLEK nonce:(NSData *)nonce;
- (NSData*) encrypt: (NSData*)dataIn;
- (NSData*) randomDataOfLength:(size_t)length;
- (NSData*) encryptWithRanIV: (NSData*)dataIn;
- (NSData*) decrypt: (NSData*)dataIn;
+ (NSData*) sha1: (NSData*)dataIn;

@end
