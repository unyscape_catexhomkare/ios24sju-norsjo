//
//  DanaTools.h
//  DanaTools
//
//  Created by Tomas Clausen on 31/01/2018.
//  Copyright © 2018 Poly-control. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DanaTools/MWMLockName.h>
#import <DanaTools/PKEncryption.h>



//! Project version number for DanaTools.
FOUNDATION_EXPORT double DanaToolsVersionNumber;

//! Project version string for DanaTools.
FOUNDATION_EXPORT const unsigned char DanaToolsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DanaTools/PublicHeader.h>


