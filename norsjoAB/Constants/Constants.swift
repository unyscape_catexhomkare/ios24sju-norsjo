//
//  Constants.swift
//  Amistos
//
//  Created by chawtech solutions on 3/26/18.
//  Copyright © 2018 chawtech solutions. All rights reserved.
//


import UIKit
import MBProgressHUD
import AVFoundation

let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
let window = UIApplication.shared.keyWindow!

let IS_IPHONE_5 = UIScreen.main.bounds.height == 568
let IS_IPHONE_6_7_8 = UIScreen.main.bounds.height == 667
let IS_IPHONE_6P_7P_8P = UIScreen.main.bounds.height == 736
let IS_IPHONE_X = UIScreen.main.bounds.height == 812

let kChatPresenceTimeInterval:TimeInterval = 45
let kDialogsPageLimit:UInt = 100
let kMessageContainerWidthPadding:CGFloat = 40.0

typealias FetchProfileResponse = (Bool) -> Void

let appDelegateInstance = UIApplication.shared.delegate as! AppDelegate
//let managedContextInstance = appDelegateInstance.persistentContainer.viewContext
//let sideMenuViewController = appDelegateInstance.window?.rootViewController as! LGSideMenuController

let kPasswordMinimumLength = 6
let kPasswordMaximumLength = 15
let kUserFullNameMaximumLength = 56
let kPhoneNumberMaximumLength = 10
let kMessageMinimumLength = 25
let kMessageMaximumLength = 250
let deviceType = "iPhone"
let deviceID = UIDevice.current.identifierForVendor?.uuidString
let selectionColor = UIColor(red: 36/255.0, green: 98/255.0, blue: 126/255.0, alpha: 1.0)

let kUnexpectedErrorAlertString = "Ett oväntat fel har uppstått. Var god försök igen." as String

// //// Staging environment////
//let BASE_URL = "http://norsjo.stagingshop.com/"
//let TRUST_BASE_URL = "https://norsjo.stagingshop.com/"
//
//let PORT_URL = "http://52.66.58.148:9095/1.0/"
//
//struct LOGIN_CREDENTIALS {
//    static let EMAIL = "sandeepvermalmp@gmail.com"
//    static let PASSWORD = "!Sandeep@01988"
//}
//////**************////


//// Live environment////
let BASE_URL = "http://3.133.239.248/norsjo/"
let TRUST_BASE_URL = "https://3.133.239.248/norsjo/"

let PORT_URL = "http://3.133.239.248:9099/1.0/"

struct LOGIN_CREDENTIALS {
    static let EMAIL = "24sjuab@gmail.com"
    static let PASSWORD = "bagvagen10"
}
////**************////


let BANKING_ID_API_URL = "api/avanza_order_ref"
let GET_IOS_PROFILE = "api/get_profile_ios"

struct PROJECT_URL {
    static let GET_PRODUCT_BY_BARCODE = "api/productApi"
    static let PAYMENT_INITIATE_API = "api/paymentInit"
    static let PAYMENT_PROCESSED_API = "api/paymentProceed"
}  

struct USER_DEFAULTS_KEYS {
//    static let BANK_ID_AUTHORIZED = "bankIdAuthorized"  // used for auto login
    static let IOS_PROFILE_RESPONSE = "iosProfileResponse"
}

//MARK:- Logout User
func logoutUser() {
//    UserDefaults.standard.set(false, forKey: USER_DEFAULTS_KEYS.BANK_ID_AUTHORIZED)
    flushUserDefaults()
    //clearImageCache()
     //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StopTimerNotification"), object: nil)
    UIApplication.shared.delegate?.window??.rootViewController = UINavigationController(rootViewController: AppDelegate.bankingIdViewController())
}

//MARK:- Remove User Defaults
func flushUserDefaults() {
    let domain = Bundle.main.bundleIdentifier!
    UserDefaults.standard.removePersistentDomain(forName: domain)
    UserDefaults.standard.synchronize()
//    NoteInstanceID.instanceID().instanceID { (result, error) in
//        if let error = error {
//            print("Error fetching remote instange ID: \(error)")
//        } else if let result = result {
//            UserDefaults.standard.setValue(result.token, forKey: USER_DEFAULTS_KEYS.FCM_Key)
//        }
//    }
} 

func initialiseAppWithController(_ controller : UIViewController) {
    let navigationController = UINavigationController(rootViewController: controller)
    navigationController.isNavigationBarHidden = true
    appDelegateInstance.window?.rootViewController = navigationController  
}

//MARK:- Alert Methods
func showInvalidInputAlert(_ fieldName : String) {
    UIAlertController.showInfoAlertWithTitle("Varna", message: String(format:"Ange en giltig %@.",fieldName), buttonTitle: "Okej")
}

//MARK:- MBProgressHUD Methods
func showProgressOnView(_ view:UIView) {
    let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.label.text = "Vänta..."
}

func showProgressOnViewWithMessage(_ view:UIView, message: String) {
    let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.label.text = message
}

func hideProgressOnView(_ view:UIView) {
    MBProgressHUD.hide(for: view, animated: true)
}

func hideAllProgressOnView(_ view:UIView) {
    MBProgressHUD.hideAllHUDs(for: view, animated: true)
}

//MARK:- Clear SDWebImage Cache
func clearImageCache() {
//    SDImageCache.shared.clearDisk()
//    SDImageCache.shared.clearMemory()
}

func getAppVersion() -> String {
    return "\(Bundle.main.infoDictionary!["CFBundleShortVersionString"] ?? "")"
}

func callBackUrl() {
    let url = URL(string: "https://app.bankid.com/?autostarttoken=null&redirect=norsjoAB")
    UIApplication.shared.open(url!, options: [.universalLinksOnly:true]) {
        (success) in
        // handle success/failure
    }
}

func getIP()-> String? {
    var address: String?
    var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
    if getifaddrs(&ifaddr) == 0 {
        var ptr = ifaddr
        while ptr != nil {
            defer { ptr = ptr?.pointee.ifa_next } // memory has been renamed to pointee in swift 3 so changed memory to pointee
            let interface = ptr?.pointee
            let addrFamily = interface?.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {  // String.fromCString() is deprecated in Swift 3. So use the following code inorder to get the exact IP Address.
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
    }
    return address
}

//MARK:-document directory realted method
public func getDirectoryPath() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

public func saveImageDocumentDirectory(usedImage:UIImage, imageName:String) {
    let fileManager = FileManager.default
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
    let imageData = usedImage.jpegData(compressionQuality: 0.5)  
    fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
}
