//
//  AppDelegate.swift
//  Norsjo AB
//
//  Created by Unyscape Infocom on 11/09/19.
//  Copyright © 2019 Unyscape Infocom. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import DanaTools

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, DLServerDelegate {
    func getServerAddress() -> String? {
        return "https://ekey-backend-prod.pre-prod.danalock.com"
    }
    
    
    var window: UIWindow?
    static var accessToken: String? = nil
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        // Override point for customization after application launch.
        let server = DLServer.sharedInstance
        //        server.address = "https://ekey-backend-prod.pre-prod.danalock.com"
        server.delegate = self
        //UserDefaults.standard.set(true, forKey: USER_DEFAULTS_KEYS.BANK_ID_AUTHORIZED)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print("get redirect url:" + "\(url.absoluteString)")
        if url.absoluteString == "norsjoAB://" {
            NotificationCenter.default.post(name: Notification.Name("bankId"), object: nil)
        }
        else {
            NotificationCenter.default.post(name: Notification.Name("swish"), object: nil)
        }
        return true
    }
    
    // DLServer delegate functions
    func getAccessToken() -> String? {
        print("Access Token: \(String(describing: AppDelegate.accessToken))")
        return AppDelegate.accessToken
    }
    
    func serverDidRecieveNew(advertisements: [DLAdvertisement]) {
        print("advertisements: \(advertisements)")
    }
}
