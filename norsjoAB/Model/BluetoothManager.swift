//
//  BridgeCentralManager.swift
//  libDanalock Demo Application
//
//  Created by TC on 02/01/17.
//  Copyright © 2017 Poly-Control. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreData
import DanaTools

class BluetoothManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    public enum BTScanState {
        case stopped
        case starting
        case scanning
    }
    
    public enum BTConnectionState {
        case closed
        case connecting
    }
    
    var centralManager: CBCentralManager!
    
    var scanState: BTScanState!
    var connectionState: BTConnectionState!
    
    var dmiService: DmiService!
    
    let dmiScanQueue = DispatchQueue(label: "com.poly-control.scan-queue")
    
    //    struct BleInfo {
    //        var peripheral: CBPeripheral
    //        var advertisementData: [String : Any]
    //        var RSSI: Int
    //    }
    
    class ScannedDevice {
        var broadcastContent: Dmi_BroadcastContent!
        let peripheral: CBPeripheral
        let id: String
        var info: DLV3DeviceInfo?
        
        init(peripheral: CBPeripheral, id: String, broadcastContent: Dmi_BroadcastContent) {
            self.peripheral = peripheral
            self.id = id
            self.broadcastContent = broadcastContent
        }
        
        func touch(broadcastContent: Dmi_BroadcastContent) {
            self.broadcastContent = broadcastContent
        }
    }
    
    var scannedDevices: [String:ScannedDevice]!
    
    var isLoadingInfo: Bool = false
    var serverRefreshingQueue: [String: ScannedDevice]!
    var serverCurrentlyLoadingList: [String : ScannedDevice]!
    
    public static let sharedInstance = BluetoothManager()
    
    public func reset() {
        scannedDevices = [:]
        serverRefreshingQueue = [:]
        serverCurrentlyLoadingList = [:]
        
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: [CBCentralManagerOptionShowPowerAlertKey: NSNumber.init(booleanLiteral: true)])
        dmiService = DmiService(manager: centralManager, localAddress: Dmi_SerialNumber(colonSeparatedString: "11:22:33:44:55:66"))
        
        scanState = .stopped
        connectionState = .closed
    }
    
    fileprivate override init() {
        super.init()
        
        reset()
    }
    
    // MARK: - Scanning
    func startScan(completion: (Bool) -> Void) {
        if centralManager.state != .poweredOn {
            print("CBCentralManager is not yet ready, scan will be started when it's state is PoweredOn")
            scanState = .starting
            completion(false)
            return
        }
        if scanState != .scanning {
            doStartBTScan()
            scanState = .scanning
        }
    }
    
    fileprivate func doStartBTScan() {
        print("Starting CBCentralManager LE scan")
        print("State of CBCentralManager is \(self.centralManager.state)")
        centralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true])
    }
    
    func stopScan() {
        if scanState == .scanning {
            print("Stop BLE scan")
            scanState = .stopped
            centralManager.stopScan()
        }
    }
    
    public var bluetoothAvaliableListener: ((Bool) -> ())?
    
    // MARK: - CBCentralmanager state
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("State of CBCentralManager changed to \(central.state)")
        if (central.state == .poweredOn) {
            bluetoothAvaliableListener?(true)
            
            if (scanState == .starting) {
                print("CBCentralManager state is now PoweredOn and program requested a LE scan while it was not ready. Going to start Bluetooth scan now.")
                doStartBTScan()
                scanState = .scanning
            }
        } else {
            bluetoothAvaliableListener?(false)
        }
    }
    
    // MARK: - CBCentralManager Connection callbacks
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        connectionState = .connecting
        peripheral.delegate = self
        peripheral.readRSSI()
    }
    
    let RSSI_TRHESHOLD = -99
    let TIME_BETWEEN_RSSI_CHECKS: Double = 1
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        if (RSSI.intValue > RSSI_TRHESHOLD) {
            dmiService.didConnect(central: centralManager, peripheral: peripheral)
        } else {
            Tools.delay(bySeconds: TIME_BETWEEN_RSSI_CHECKS) { () -> () in
                peripheral.delegate = self
                peripheral.readRSSI()
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        connectionState = .closed
        dmiService.didDisconnectPeripheral(peripheral: peripheral, error: error as NSError?)
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        connectionState = .closed
        dmiService.didFailToConnect(peripheral: peripheral, error: error as NSError?)
    }
    
    // MARK: - Scanning
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        //        print("didDiscover peripheral \(peripheral.name) \(peripheral.identifier)")
        
        if (RSSI.intValue > RSSI_TRHESHOLD) {
            
            let prot = Toolbox.deviceType(advertisementData: advertisementData)
            
            if prot == .DMI {
                if let data = advertisementData[CBAdvertisementDataManufacturerDataKey] as? Data {
                    if let broadcastContent = Dmi_BroadcastReceiver.create(fromAdvertisement: data) {
                        if let serial = broadcastContent.serialNumber.colonSeparatedString() {
                            if (serverRefreshingQueue[serial] == nil && serverCurrentlyLoadingList[serial] == nil) {
                                handleScannedDevice(id: serial, peripheral: peripheral, broadcastContent: broadcastContent)
                            }
                        }
                    }
                }
            } else {
                //                print("protocol is not DMI. \(prot)")
            }
        }
    }
    
    func handleScannedDevice(id: String, peripheral: CBPeripheral, broadcastContent: Dmi_BroadcastContent) {
        if let device = scannedDevices[id] { // update nearby devices
            device.touch(broadcastContent: broadcastContent)
        } else {
            dmiScanQueue.sync {
                self.handleNewDevice(id: id, peripheral: peripheral, broadcastContent: broadcastContent)
            }
        }
    }
    
    func handleNewDevice(id: String, peripheral: CBPeripheral, broadcastContent: Dmi_BroadcastContent) {
        if (serverCurrentlyLoadingList[id] == nil && serverRefreshingQueue[id] == nil) {
            serverRefreshingQueue[id] = ScannedDevice(peripheral: peripheral, id: id, broadcastContent: broadcastContent)
        }
        
        getV3InfoFromQueue()
    }
    
    func getV3InfoFromQueue() {
        if (!isLoadingInfo) {
            isLoadingInfo = true
            
            serverCurrentlyLoadingList = serverRefreshingQueue
            serverRefreshingQueue = [:]
            
            var deviceIds = [[String:AnyObject]]()
            for device in serverCurrentlyLoadingList {
                var entry: [String:AnyObject] = [:]
                entry["serial_number"] = device.key as AnyObject
                deviceIds.append(entry)
            }
            
            DLServer.sharedInstance.V3DevicesInfo(serials: deviceIds) { (response: DLV3DevicesInfoResponse) -> () in
                print(response.responseCode)
                if (response.success) {
                    self.dmiScanQueue.sync {
                        for info in response.informations {
                            if let scannedDevice = self.serverCurrentlyLoadingList[info.key] {
                                scannedDevice.info = info.value
                                self.scannedDevices[info.key] = scannedDevice
                            }
                        }
                        self.serverCurrentlyLoadingList = [:]
                        self.isLoadingInfo = false
                        
                        if (!self.serverRefreshingQueue.isEmpty) {
                            self.getV3InfoFromQueue()
                        }
                    }
                }
            }
            print("scannedDevices with info: \(String(describing: scannedDevices))")
        }
    }
    
}

