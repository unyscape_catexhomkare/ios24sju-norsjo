//
//  DmiService.swift
//
//  Created by Tomas Clausen on 01/24/2017.
//  Copyright © 2017 Poly-Control. All rights reserved.
//

import Foundation
import CoreBluetooth

typealias Error_continuation = (NSError?) -> ()

extension Data {
    var hexDescriptionn: String {
        return reduce("") {$0 + String(format: "%02x", $1)}
    }
}

class DmiService: NSObject, CBPeripheralDelegate {
    
    let centralManager: CBCentralManager!
    var peripheral: CBPeripheral?
    var serial_number: Dmi_SerialNumber?
    var continuation: Error_continuation?
    
    var service: CBService?
    var dmiTx:CBCharacteristic?, dmiRx: CBCharacteristic?
    
    let connectTimeout: Double = 15.0
    var connectTimer: Timer?
    
    public func setLocalAddress(address: Dmi_SerialNumber) {
        if (self.peripheral == nil) {
            Dmi.setLocalAddress(address)
        }
    }
    
    internal init(manager: CBCentralManager, localAddress: Dmi_SerialNumber) {
        self.centralManager = manager
        
        super.init()
        
        Dmi.setLocalAddress(localAddress)
        Dmi.initialize()
        
//        Dmi.setLogMessageGeneratedCallback { (message: String?) -> () in
//            if let message = message {
//                print(message)
//            }
//        }
        
        Dmi.setDataGeneratedCallback { (id, data) -> dmi_status in
            guard let p = self.peripheral else {
                return DMI_STATUS_ERROR
            }
            
            guard let data = data else {
                return DMI_STATUS_ERROR
            }
            
            let nsData = NSData(data: data)
            if let dmiTx = self.dmiTx {
                let properties = dmiTx.properties
                var fromIndex = 0
                
                while nsData.length > fromIndex {
                    let toIndex = min(20, nsData.length - fromIndex) + fromIndex
                    let chunk = nsData.subdata(with: NSMakeRange(fromIndex, toIndex - fromIndex))
                    if properties.intersection(.writeWithoutResponse) != [] {
                        p.writeValue(chunk, for: dmiTx, type: .withoutResponse)
                    } else {
                        p.writeValue(chunk, for: dmiTx, type: .withResponse)
                    }
                    fromIndex = toIndex
                }
                return DMI_STATUS_OK
            } else {
                return DMI_STATUS_ERROR
            }
        }
    }
    
    func connect( peripheral: CBPeripheral, remoteSerialNumber: Dmi_SerialNumber, continuation: @escaping Error_continuation ) {
        
        self.continuation = continuation
        
        guard let centralManager = self.centralManager else {
            self.didFailToConnect(peripheral: peripheral, error: NSError(domain: "DMIService", code: 1, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Failed to connect to your device, please try again.", comment: "")]))
            return
        }
        
        self.service = nil
        self.dmiTx = nil
        self.dmiRx = nil
        self.serial_number = remoteSerialNumber
        
        centralManager.connect(peripheral, options: nil)
        
        if connectTimeout > 0 {
            connectTimer?.invalidate()
            connectTimer = Timer.scheduledTimer(timeInterval: connectTimeout, target: self, selector: #selector(timeout), userInfo: peripheral, repeats: false)
            RunLoop.main.add(connectTimer!, forMode: RunLoop.Mode.default)
        }
    }
    
    @objc func timeout(timer: Timer) {
        if let peripheral = timer.userInfo as? CBPeripheral {
            didFailToConnect(peripheral: peripheral, error: NSError(domain: "DMIService", code: 2, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("The bluetooth connection timed out, please try again.", comment: "")]))
        }
        invalidateTimeoutTimer()
    }
    
    private func invalidateTimeoutTimer() {
        connectTimer?.invalidate()
        connectTimer = nil
    }
    
    func disconnect(continuation: @escaping Error_continuation) {
        self.invalidateTimeoutTimer()
        self.continuation = continuation
        
        let disconnectStatus = Dmi_AfiClient.disconnect(continuation: {status in
            print("afi diconnected continuation, status: \(status)")
        })
        print("disconnectStatus: \(disconnectStatus)")
        if (disconnectStatus != DMI_STATUS_OK) {
            guard let centralManager = self.centralManager else {
                continuation(NSError(domain: "DMIService", code: Int(disconnectStatus.rawValue), userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("An app error occured during disconnect.", comment: "")]))
                return
            }
            
            guard let peripheral = self.peripheral else {
                continuation(NSError(domain: "DMIService", code: 3, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("An app error occured during disconnect.", comment: "")]))
                return
            }
            
            centralManager.cancelPeripheralConnection(peripheral)
        }
    }
    
    // - MARK: Callbacks
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("didDiscoverServices")
        if let error = error {
            self.didFailToConnect(peripheral: peripheral, error: error as NSError)
        } else {
            for service in peripheral.services! {
                if service.uuid.uuidString == Dmi.uartServiceUUID().uuidString {
                    self.service = service
                    peripheral.discoverCharacteristics([CBUUID(nsuuid: Dmi.readCharacteristicUUID()), CBUUID(nsuuid: Dmi.writeCharacteristicUUID())], for: service)
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("didDiscoverCharacteristics")
        
        if let error = error {
            print("didDiscoverCharacteristics error: \(error)")
            self.didFailToConnect(peripheral: peripheral, error: error as NSError)
        } else {
            guard let characteristics = service.characteristics else {
                print("didDiscoverCharacteristics service has no characteristics")
                self.didFailToConnect(peripheral: peripheral, error: NSError(domain: "DMIService", code: 4, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("An app error occured while connecting to your device, please try again.", comment: "")]))
                return
            }
            
            for chr in characteristics {
                if (chr.uuid.uuidString == Dmi.readCharacteristicUUID().uuidString) {
                    self.dmiRx = chr
                } else if (chr.uuid.uuidString == Dmi.writeCharacteristicUUID().uuidString) {
                    self.dmiTx = chr
                }
            }
            
            guard let _ = self.dmiTx, let dmiRx = self.dmiRx else {
                print("didDiscoverCharacteristics dmiTx or dmiRx not set")
                self.didFailToConnect(peripheral: peripheral, error: NSError(domain: "DMIService", code: 5, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("An app error occured while connecting to your device, please try again.", comment: "")]))
                return
            }
            peripheral.setNotifyValue(true, for: dmiRx)
            
            if let serialNumber = self.serial_number {
                guard let continuation = self.continuation else {
                    print("didDiscoverCharacteristicsFor \(peripheral), no continuation found")
                    return
                }
                
                let connectStatus = Dmi_AfiClient.connect(serialNumber, continuation: { (status) in
                    DispatchQueue.main.async(){
                        if status == DMI_STATUS_OK {
                            self.invalidateTimeoutTimer()
                            self.continuation = nil
                            continuation(nil) // Success, now connected
                        } else {
                            print("didDiscoverCharacteristics status != DMI_STATUS_OK \(status.rawValue)")
                            self.didFailToConnect(peripheral: peripheral, error: NSError(domain: "DMIService", code: Int(status.rawValue), userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Failed to connect to device, please try again.", comment: "")]))
                        }
                    }
                })
                
                if (connectStatus != DMI_STATUS_OK) {
                    print("didDiscoverCharacteristics connectStatus != DMI_STATUS_OK \(connectStatus.rawValue)")
                    self.didFailToConnect(peripheral: peripheral, error: NSError(domain: "DMIService", code: Int(connectStatus.rawValue), userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Failed to connect to device, please try again.", comment: "")]))
                }
            } else {
                print("didDiscoverCharacteristics - Could not find provided dmi_serial")
                self.didFailToConnect(peripheral: peripheral, error: NSError(domain: "DMIService", code: 6, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("An app error occured while connecting to your device, please try again.", comment: "")]))
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            if let continuation = self.continuation {
                self.continuation = nil
                continuation(error as NSError)
            }
        }

    }

    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        if let error = error {
            if let continuation = self.continuation {
                self.continuation = nil
                continuation(error as NSError)
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            if let continuation = self.continuation {
                self.continuation = nil
                continuation(error as NSError)
            }
        } else {
            let data = characteristic.value
            Dmi.dataReceived(data)
        }
    }

    // - MARK: event handlers for DmiService
    func didConnect(central: CBCentralManager, peripheral: CBPeripheral) {
        print("DmiService - didConnect to peripheral: \(peripheral)")

        self.peripheral = peripheral
        self.peripheral!.delegate = self
        let uuid = Dmi.uartServiceUUID()
        self.peripheral!.discoverServices([CBUUID(nsuuid: uuid!)])
    }
    
    func didFailToConnect(peripheral: CBPeripheral, error: NSError?) {
        self.invalidateTimeoutTimer()
        print("didFailToConnect peripheral with error: \(String(describing: error))")
        print("Dmi_AfiClient.lastError() \(Dmi_AfiClient.lastError())")
        self.peripheral = nil
        if let continuation = self.continuation {
            self.continuation = nil
            continuation(error)
        }
        
        self.centralManager.cancelPeripheralConnection(peripheral)
    }
    
    func didDisconnectPeripheral( peripheral: CBPeripheral, error: NSError?) {
        print("didDisconnectPeripheral with error: \(String(describing: error))")
        print("Dmi_AfiClient.lastError() \(Dmi_AfiClient.lastError())")
        self.peripheral = nil
        if let continuation = self.continuation {
            self.continuation = nil
            if error?.code == 7 {
                continuation(nil)
            } else {
                continuation(error)
            }
        }
    }
}
