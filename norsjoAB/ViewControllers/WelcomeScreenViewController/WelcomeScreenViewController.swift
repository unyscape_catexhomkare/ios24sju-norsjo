//
//  WelcomeScreenViewController.swift
//  norsjoAB
//
//  Created by Unyscape Infocom on 12/09/19.
//  Copyright © 2019 Unyscape Infocom. All rights reserved.
//

import UIKit

class WelcomeScreenViewController: UIViewController {
    @IBOutlet weak var customNavigationBar: CustomNavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavigationBar.titleLabel.text = "24sju AB"
        customNavigationBar.hideLeftBarButtonItem()
    }
    
    @IBAction func goForShoppingBtnTap(_ sender: Any) {
      self.navigationController?.pushViewController(AppDelegate.productListViewController(), animated: true)
    }
}        
