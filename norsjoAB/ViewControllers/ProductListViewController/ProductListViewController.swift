//
//  ProductListViewController.swift
//  norsjoAB
//
//  Created by Unyscape Infocom on 13/09/19.
//  Copyright © 2019 Unyscape Infocom. All rights reserved.
//

import UIKit
import BarcodeScanner

struct ProductStruct {
    var id : String
    var product_id : String
    var barcode : String
    var product_name : String
    var product_desc : String
    var price : String
    var units : String
    var productTotalPrice : String
}

class ProductListViewController: UIViewController {
    @IBOutlet weak var customNavigationBar: CustomNavigationBar!
    @IBOutlet weak var productListTableView: UITableView!
    @IBOutlet weak var totalPriceLbl: UILabel!
    var productList : [ProductStruct] = []
    var totalPrice : Double = 0.0
    var tokenStr : String = ""
    var urlStr : String = ""
    
    enum StringConstants: String {
        case Host = "paymentrequest"
        case SwishUrl = "swish://"
        case MerchantCallbackUrl = "norsjoABPayment://"
        case Scheme = "swish"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavigationBar.titleLabel.text = "24sju AB"
        customNavigationBar.hideLeftBarButtonItem()
        productListTableView.tableFooterView = UIView()
        productListTableView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.swishAppPaymentCompleted(notification:)), name: Notification.Name("swish"), object: nil)
    }
    
    @objc func swishAppPaymentCompleted(notification: Notification) {
        hideAllProgressOnView(self.view)
        self.paymentProcessingApi(url: urlStr)
    }
    
    @IBAction func scanBarCodeBtnTap(_ sender: Any) {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        viewController.headerViewController.closeButton.tintColor = .white
        viewController.headerViewController.navigationBar.barTintColor = UIColor.init(red: 1.0, green: 88/255, blue: 1.0, alpha: 1.0)
        present(viewController, animated: true, completion: nil)  
    }
    
    @IBAction func checkOutBtnTap(_ sender: Any) {
        if totalPrice > 0 {
            self.paymentInitiateApi(total:totalPrice)
        }
        else {
            UIAlertController.showInfoAlertWithTitle("Varna", message: "Lägg till kvantitet att köpa.", buttonTitle: "Okej")
        }
    }
    
    func productByBarCodeApi(barCodeStr : String) {
        if Reachability.isConnectedToNetwork() {
            let param1:[String:String] = ["barcode" : barCodeStr]
            print(param1)
            showProgressOnView(self.view)
            ServerClass.sharedInstance.sendMultipartRequestToServerWithParameter(urlString: BASE_URL + PROJECT_URL.GET_PRODUCT_BY_BARCODE, sendJson: param1, successBlock: { (json) in
                hideAllProgressOnView(self.view)
                let status = json["status"].stringValue
                if status  == "200" {
                    print(json)
                    self.productList.append(ProductStruct.init(id: json["data"]["id"].stringValue, product_id: json["data"]["product_id"].stringValue, barcode: json["data"]["barcode"].stringValue, product_name: json["data"]["product_name"].stringValue, product_desc: json["data"]["product_desc"].stringValue, price: json["data"]["price"].stringValue, units: json["data"]["units"].stringValue, productTotalPrice: "0.0"))
                    self.productListTableView.reloadData()
                    
                    var totalPrice : Double = 0.0
                    for i in 0..<self.productList.count {
                        totalPrice = totalPrice + Double(self.productList[i].productTotalPrice)!
                    }
                    self.totalPriceLbl.text = "Totalt: \(totalPrice) Kr"
                }
                else {
                    hideAllProgressOnView(self.view)
                    UIAlertController.showInfoAlertWithTitle("Meddelande", message: json["status"].stringValue, buttonTitle: "Okej")
                }
            }, errorBlock: { (NSError) in
                hideAllProgressOnView(self.view)
                UIAlertController.showInfoAlertWithTitle("Varna", message: kUnexpectedErrorAlertString, buttonTitle: "Okej")
            })
        }
        else{
            hideAllProgressOnView(self.view)
            UIAlertController.showInfoAlertWithTitle("Varna", message: "Kontrollera internetanslutning", buttonTitle: "Okej")
        }
    }
    
    func paymentInitiateApi(total : Double) {
        if Reachability.isConnectedToNetwork() {
            let param1:[String:String] = [
                "payeePaymentReference" : "123456",
                "payeeAlias" : "1236569453",
                "amount" : "\(totalPrice)",
                "message" : "Payment Initiation Started...."
            ]
            print(param1)
            showProgressOnView(self.view)
            ServerClass.sharedInstance.sendMultipartRequestToServerWithParameter(urlString: BASE_URL + PROJECT_URL.PAYMENT_INITIATE_API, sendJson: param1, successBlock: { (json) in
                hideAllProgressOnView(self.view)
                let status = json["status"].stringValue
                if status  == "200" {
                    print(json)
                    self.tokenStr = json["PaymentRequestToken"].stringValue
                    self.urlStr = json["data"].stringValue
                    //UIAlertController.showInfoAlertWithTitle("Meddelande", message: json["message"].stringValue, buttonTitle: "Okej")
                    self.openSwishAppWithToken()
                }
                else {
                    hideAllProgressOnView(self.view)
                    UIAlertController.showInfoAlertWithTitle("Meddelande", message: "Fel!!!", buttonTitle: "Okej")
                }
            }, errorBlock: { (NSError) in
                hideAllProgressOnView(self.view)
                UIAlertController.showInfoAlertWithTitle("Varna", message: kUnexpectedErrorAlertString, buttonTitle: "Okej")
            })
        }
        else{
            hideAllProgressOnView(self.view)
            UIAlertController.showInfoAlertWithTitle("Varna", message: "Kontrollera internetanslutning", buttonTitle: "Okej")
        }
    }
    
    func openSwishAppWithToken() {
        guard isSwishAppInstalled() else {
            UIAlertController.showInfoAlertWithTitle("Varna", message: "Swish-APPEN är inte installerad.", buttonTitle: "Okej")
            return
        }
        
        guard let callback = encodedCallbackUrl() else {
            preconditionFailure("Callback url is required")
        }
        
        var urlComponents = URLComponents()
        urlComponents.host = StringConstants.Host.rawValue
        urlComponents.scheme = StringConstants.Scheme.rawValue
        urlComponents.queryItems = [URLQueryItem(name: "token", value: tokenStr),
                                    URLQueryItem(name: "callbackurl", value: callback)]
        
        guard let url = urlComponents.url else {
            preconditionFailure("Invalid url")
        }
        
        DispatchQueue.main.async {
            if UIApplication.shared.canOpenURL(url) {
                print("open url" + "\(url)")
                UIApplication.shared.open(url, options: [:]) {
                    (success) in
                    print(success)
                    
                }
            }
        }
    }
    
    func encodedCallbackUrl() -> String? {
        let callback = StringConstants.MerchantCallbackUrl.rawValue
        let disallowedCharacters = NSCharacterSet(charactersIn: "!*'();:@&=+$,/?%#[]")
        let allowedCharacters = disallowedCharacters.inverted
        return callback.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
    
    func isSwishAppInstalled() -> Bool {
        guard let url = URL(string: StringConstants.SwishUrl.rawValue) else {
            preconditionFailure("Invalid url")
        }
        return UIApplication.shared.canOpenURL(url)
    }
    
    func paymentProcessingApi(url:String) {
        if Reachability.isConnectedToNetwork() {
            let param1:[String:String] = [
                "payment_url" : url  
            ]
            print(param1)
            showProgressOnView(self.view)
            ServerClass.sharedInstance.sendMultipartRequestToServerWithParameter(urlString: BASE_URL + PROJECT_URL.PAYMENT_PROCESSED_API, sendJson: param1, successBlock: { (json) in
                hideAllProgressOnView(self.view)
                let status = json["status"].stringValue
                if status  == "200" {
                    print(json)
                    if json["data"]["status"].stringValue == "CREATED" {
                        UIAlertController.showInfoAlertWithTitle("Meddelande", message: "Betalningsbehandling api kallas", buttonTitle: "Okej")
                    }
                    else if json["data"]["status"].stringValue == "PAID" {
                        self.navigationController?.pushViewController(AppDelegate.thankYouViewController(), animated: true)
                    }
                }
                else {
                    hideAllProgressOnView(self.view)
                    UIAlertController.showInfoAlertWithTitle("Meddelande", message: json["status"].stringValue, buttonTitle: "Okej")
                }
            }, errorBlock: { (NSError) in
                hideAllProgressOnView(self.view)
                UIAlertController.showInfoAlertWithTitle("Varna", message: kUnexpectedErrorAlertString, buttonTitle: "Okej")
            })
        }
        else{
            hideAllProgressOnView(self.view)
            UIAlertController.showInfoAlertWithTitle("Varna", message: "Kontrollera internetanslutning", buttonTitle: "Okej")
        }
    }
}

extension ProductListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProductListTableViewCell") as! ProductListTableViewCell
        cell.productNameLbl.text = self.productList[indexPath.row].product_name
        cell.priceLbl.text = "pris: " + self.productList[indexPath.row].price
        cell.quantityLbl.text = self.productList[indexPath.row].units
        cell.totalPriceLbl.text = "Totalt: " + self.productList[indexPath.row].productTotalPrice + " Kr"
        
        cell.plusBtn.addTarget(self, action: #selector(plusBtnTap(sender:)), for: .touchUpInside)
        cell.plusBtn.tag = indexPath.row
        cell.minusBtn.addTarget(self, action: #selector(minusBtnTap(sender:)), for: .touchUpInside)
        cell.minusBtn.tag = indexPath.row
        return cell
    }
    
    @objc func plusBtnTap(sender: UIButton){
        self.productList[sender.tag].units = "\(Int(self.productList[sender.tag].units)! + 1)"
        self.productList[sender.tag].productTotalPrice = "\(Double(self.productList[sender.tag].units)! * Double(self.productList[sender.tag].price)!)"
        self.productListTableView.reloadData()
        
        totalPrice = 0.0
        for i in 0..<self.productList.count {
            totalPrice = totalPrice + Double(self.productList[i].productTotalPrice)!
        }
        self.totalPriceLbl.text = "Totalt: \(totalPrice) Kr"
    }
    
    @objc func minusBtnTap(sender: UIButton){
        if Int(self.productList[sender.tag].units)! > 0 {
            self.productList[sender.tag].units = "\(Int(self.productList[sender.tag].units)! - 1)"
        }
        self.productList[sender.tag].productTotalPrice = "\(Double(self.productList[sender.tag].units)! * Double(self.productList[sender.tag].price)!)"
        self.productListTableView.reloadData()
        
        totalPrice = 0.0
        for i in 0..<self.productList.count {
            totalPrice = totalPrice + Double(self.productList[i].productTotalPrice)!
        }
        self.totalPriceLbl.text = "Totalt: \(totalPrice) Kr"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
}

extension ProductListViewController: BarcodeScannerCodeDelegate, BarcodeScannerDismissalDelegate, BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        controller.dismiss(animated: true, completion: nil)
        var checkExist = false
        for i in 0..<self.productList.count {
            if self.productList[i].barcode == code {
                self.productList[i].units = "\(Int(self.productList[i].units)! + 1)"
                self.productList[i].productTotalPrice = "\(Double(self.productList[i].units)! * Double(self.productList[i].price)!)"
                self.productListTableView.reloadData()
                
                totalPrice = 0.0
                for i in 0..<self.productList.count {
                    totalPrice = totalPrice + Double(self.productList[i].productTotalPrice)!
                }
                self.totalPriceLbl.text = "Totalt: \(totalPrice) Kr"
                checkExist = true
                break
            }
        }
        
        if checkExist == false {
            self.productByBarCodeApi(barCodeStr: code)
        }
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        UIAlertController.showInfoAlertWithTitle("Fel", message: error.localizedDescription, buttonTitle: "Okej")
    }
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
