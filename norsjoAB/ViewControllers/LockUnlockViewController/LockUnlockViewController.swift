//
//  LockUnlockViewController.swift
//  norsjoAB
//
//  Created by Unyscape Infocom on 12/09/19.
//  Copyright © 2019 Unyscape Infocom. All rights reserved.
//

import UIKit
import DanaTools

class LockUnlockViewController: UIViewController {
    @IBOutlet weak var customNavigationBar: CustomNavigationBar!
    @IBOutlet weak var lockUnlockImageView: UIImageView!
    @IBOutlet weak var unlockBtn: UIButton!
    @IBOutlet weak var lockBtn: UIButton!
    var myKeys: [DLV3Key] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavigationBar.titleLabel.text = "24sju AB"
        customNavigationBar.hideLeftBarButtonItem()
    }
    
    func getLockUnlockStatus() {
        self.myKeys.removeAll()
        showProgressOnView(self.view)
        DLServer.sharedInstance.downloadKeys(username: LOGIN_CREDENTIALS.EMAIL) { (response: DLGetKeysResponse) in
            if response.success {
                print("responsecode: \(response.responseCode)")
                print("keys: \(response.keys)")
                for key in response.keys {
                    if let v3Key = key as? DLV3Key {
                        self.myKeys.append(v3Key)
                    }
                }  
                
                if self.myKeys.count > 0 {
                    print("Total Keys \(self.myKeys.count)")
                    if let device = BluetoothManager.sharedInstance.scannedDevices[self.myKeys[0].id.lowercased()], let advertisingKeyData = Data(base64Encoded: self.myKeys[0].advertisingKey, options: []), let decrypted = device.broadcastContent.decrypt(withKey: advertisingKeyData), let genDevice = decrypted.decodeAsGeneric() {
                        hideAllProgressOnView(self.view)
                        print(genDevice.lockIsLocked)
                        if genDevice.lockIsLocked == true {
                            //                            self.lockBtn.isUserInteractionEnabled = false
                            //                            self.unlockBtn.isUserInteractionEnabled = true
                            //                            self.lockBtn.alpha = 0.5
                            //                            self.unlockBtn.alpha = 1.0
                        }
                        else {
                            //                            self.unlockBtn.isUserInteractionEnabled = false
                            //                            self.lockBtn.isUserInteractionEnabled = true
                            //                            self.unlockBtn.alpha = 0.5
                            //                            self.lockBtn.alpha = 1.0
                        }
                    }
                    else {
                        hideAllProgressOnView(self.view)
                        UIAlertController.showInfoAlertWithTitle("Varna", message: "Lås status hittades inte.", buttonTitle: "Okej")
                    }
                }
                else {
                    hideAllProgressOnView(self.view)
                    UIAlertController.showInfoAlertWithTitle("Varna", message: "Inga nycklar hittades.", buttonTitle: "Okej")
                }
            }
            else {
                hideAllProgressOnView(self.view)
                UIAlertController.showInfoAlertWithTitle("Varna", message: "Det gick inte att hitta någon nyckel.", buttonTitle: "Okej")
            }
        }
    }
    
    @IBAction func unlockBtnTap(_ sender: Any) {
        self.myKeys.removeAll()
        showProgressOnViewWithMessage(self.view, message: "Upplåsning...")
        DLServer.sharedInstance.downloadKeys(username: LOGIN_CREDENTIALS.EMAIL) { (response: DLGetKeysResponse) in
            if response.success {
                print("responsecode: \(response.responseCode)")
                print("keys: \(response.keys)")
                for key in response.keys {
                    if let v3Key = key as? DLV3Key {
                        self.myKeys.append(v3Key)
                    }
                }
                
                if self.myKeys.count > 0 {
                    print("Total Keys \(self.myKeys.count)")
                    if let device = BluetoothManager.sharedInstance.scannedDevices[self.myKeys[0].id.lowercased()], let advertisingKeyData = Data(base64Encoded: self.myKeys[0].advertisingKey, options: []), let decrypted = device.broadcastContent.decrypt(withKey: advertisingKeyData), let genDevice = decrypted.decodeAsGeneric() {
                        hideAllProgressOnView(self.view)
                        print(genDevice.lockIsLocked)
                        if genDevice.lockIsLocked == true {
                            self.unlockMethod()
                        }
                        else {
                            self.callWelcomeScreen()
                        }
                    }
                    else {
                        hideAllProgressOnView(self.view)
                        UIAlertController.showInfoAlertWithTitle("Varna", message: "Lås status hittades inte.", buttonTitle: "Okej")
                    }
                }
                else {
                    hideAllProgressOnView(self.view)
                    UIAlertController.showInfoAlertWithTitle("Varna", message: "Inga nycklar hittades.", buttonTitle: "Okej")
                }
            }
            else {
                hideAllProgressOnView(self.view)
                UIAlertController.showInfoAlertWithTitle("Varna", message: "Det gick inte att hitta någon nyckel.", buttonTitle: "Okej")
            }
        }
    }
    
    @IBAction func lockBtnTap(_ sender: Any) {
        self.myKeys.removeAll()
        showProgressOnViewWithMessage(self.view, message: "Låsning...")
        DLServer.sharedInstance.downloadKeys(username: LOGIN_CREDENTIALS.EMAIL) { (response: DLGetKeysResponse) in
            if response.success {
                print("responsecode: \(response.responseCode)")
                print("keys: \(response.keys)")
                for key in response.keys {
                    if let v3Key = key as? DLV3Key {
                        self.myKeys.append(v3Key)
                    }
                }
                
                if self.myKeys.count > 0 {
                    print("Total Keys \(self.myKeys.count)")
                    if let device = BluetoothManager.sharedInstance.scannedDevices[self.myKeys[0].id.lowercased()], let advertisingKeyData = Data(base64Encoded: self.myKeys[0].advertisingKey, options: []), let decrypted = device.broadcastContent.decrypt(withKey: advertisingKeyData), let genDevice = decrypted.decodeAsGeneric() {
                        hideAllProgressOnView(self.view)
                        print(genDevice.lockIsLocked)
                        if genDevice.lockIsLocked == true {
                            UIAlertController.showInfoAlertWithTitle("Varna", message: "Låset är redan låst.", buttonTitle: "Okej")
                        }
                        else {
                            self.lockMethod()
                        }
                    }
                    else {
                        hideAllProgressOnView(self.view)
                        UIAlertController.showInfoAlertWithTitle("Varna", message: "Lås status hittades inte.", buttonTitle: "Okej")
                    }
                }
                else {
                    hideAllProgressOnView(self.view)
                    UIAlertController.showInfoAlertWithTitle("Varna", message: "Inga nycklar hittades.", buttonTitle: "Okej")
                }
            }
            else {
                hideAllProgressOnView(self.view)
                UIAlertController.showInfoAlertWithTitle("Varna", message: "Det gick inte att hitta någon nyckel.", buttonTitle: "Okej")
            }
        }
    }
    
    func lockMethod() {
        showProgressOnViewWithMessage(self.view, message: "Låsning...")
        guard let device = BluetoothManager.sharedInstance.scannedDevices[self.myKeys[0].id.lowercased()] else {
            UIAlertController.showInfoAlertWithTitle("Fel", message: "Ingen apparat", buttonTitle: "Okej")
            return
        }
        
        guard let serial = Dmi_SerialNumber(colonSeparatedString: self.myKeys[0].id) else {
            UIAlertController.showInfoAlertWithTitle("Fel", message: "Noserial", buttonTitle: "Okej")
            return
        }
        
        BluetoothManager.sharedInstance.dmiService.connect(peripheral: device.peripheral, remoteSerialNumber: serial, continuation: { (error) in
            if let error = error {
                UIAlertController.showInfoAlertWithTitle("Fel", message: "Misslyckades med att ansluta: \(error)", buttonTitle: "Okej")
                return
            }
            
            guard let loginTokenData = Data(base64Encoded: self.myKeys[0].loginToken, options: []) else {
                UIAlertController.showInfoAlertWithTitle("Fel", message: "Ingen nyckeldata", buttonTitle: "Okej")
                self.disconnect()
                return
            }
            
            Dmi_AfiClient_Authority.login(loginTokenData, continuation: {(status) -> Void in
                if status != DMI_STATUS_OK {
                    self.disconnect()
                    UIAlertController.showInfoAlertWithTitle("Fel", message: "kunde inte logga in: \(status)", buttonTitle: "Okej")
                    return
                }
                else {
                    Dmi_AfiClient_Lock.lock({ (status) in
                        if status != DMI_STATUS_OK {
                            hideAllProgressOnView(self.view)
                            UIAlertController.showInfoAlertWithTitle("Fel", message: "Gick inte att låsa: \(status)", buttonTitle: "Okej")
                        }
                        else {
                            UIAlertController.showInfoAlertWithTitle("Framgång", message: "Låst framgångsrikt", buttonTitle: "Okej")
                            self.getLockUnlockStatus()
                        }
                        self.disconnect()
                    })
                }
            })
        })
    }
    
    func unlockMethod() {
        showProgressOnViewWithMessage(self.view, message: "Upplåsning...")
        guard let device = BluetoothManager.sharedInstance.scannedDevices[self.myKeys[0].id.lowercased()] else {
            UIAlertController.showInfoAlertWithTitle("Fel", message: "Ingen apparat", buttonTitle: "Okej")
            return
        }
        
        guard let serial = Dmi_SerialNumber(colonSeparatedString: self.myKeys[0].id) else {
            UIAlertController.showInfoAlertWithTitle("Fel", message: "Noserial", buttonTitle: "Okej")
            return
        }
        
        BluetoothManager.sharedInstance.dmiService.connect(peripheral: device.peripheral, remoteSerialNumber: serial, continuation: { (error) in
            if let error = error {
                UIAlertController.showInfoAlertWithTitle("Fel", message: "Misslyckades med att ansluta: \(error)", buttonTitle: "Okej")
                return
            }
            
            guard let loginTokenData = Data(base64Encoded: self.myKeys[0].loginToken, options: []) else {
                UIAlertController.showInfoAlertWithTitle("Fel", message: "Ingen nyckeldata", buttonTitle: "Okej")
                self.disconnect()
                return
            }
            
            Dmi_AfiClient_Authority.login(loginTokenData, continuation: {(status) -> Void in
                if status != DMI_STATUS_OK {
                    self.disconnect()
                    UIAlertController.showInfoAlertWithTitle("Fel", message: "kunde inte logga in: \(status)", buttonTitle: "Okej")
                    return
                }
                else {
                    Dmi_AfiClient_Lock.unlock({ (status) in
                        if status != DMI_STATUS_OK {
                            hideAllProgressOnView(self.view)
                            UIAlertController.showInfoAlertWithTitle("Fel", message: "Gick inte att låsa upp: \(status)", buttonTitle: "Okej")
                        }
                        else {
                            //                            self.getLockUnlockStatus()
                            self.callWelcomeScreen()
                        }
                        self.disconnect()
                    })
                }
            })
        })
    }
    
    private func disconnect() {
        hideAllProgressOnView(self.view)
        BluetoothManager.sharedInstance.dmiService.disconnect { (error: NSError?) -> () in
            print("disconnected: \(String(describing: error))")
        }
    }
    
    func callWelcomeScreen() {
        hideAllProgressOnView(self.view)
        self.navigationController?.pushViewController(AppDelegate.welcomeScreenViewController(), animated: true)
    }
}
