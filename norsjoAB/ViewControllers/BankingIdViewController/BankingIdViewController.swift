//
//  ViewController.swift
//  Norsjo AB
//
//  Created by Unyscape Infocom on 11/09/19.
//  Copyright © 2019 Unyscape Infocom. All rights reserved.
//

import UIKit
import DanaTools

class BankingIdViewController: UIViewController {
    
    @IBOutlet weak var customNavigationBar: CustomNavigationBar!
    @IBOutlet weak var bankingIdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationBar.titleLabel.text = "24sju AB"
        customNavigationBar.hideLeftBarButtonItem() 
        
//        if UserDefaults.standard.bool(forKey: USER_DEFAULTS_KEYS.BANK_ID_AUTHORIZED) == true {
//            self.loginWithDLServer()
//        }
//        else {
            NotificationCenter.default.addObserver(self, selector: #selector(self.bankIdAuthorized(notification:)), name: Notification.Name("bankId"), object: nil)
//        }
        //194301021111
        //268740 
    }
     
    @objc func bankIdAuthorized(notification: Notification) {
        self.perform(#selector(getiOSResponse), with: nil, afterDelay: 5.0)  
    }
    
    @objc func getiOSResponse() {
        hideAllProgressOnView(self.view)
        if Reachability.isConnectedToNetwork() {
            showProgressOnView(self.view)
            ServerClass.sharedInstance.getRequestWithUrlParameters([:], path: PORT_URL + GET_IOS_PROFILE + "?device_id=\(UIDevice.current.identifierForVendor!.uuidString)", successBlock: { (json) in
                hideAllProgressOnView(self.view)
                let status = json["response"]["status"].stringValue
                if status  == "200" {
                    print(json)
                    let dict:[String:String] = [
                        "email":json["response"]["data"]["email"].stringValue,
                        "profile_pic":json["response"]["data"]["profile_pic"].stringValue,
                        "name":json["response"]["data"]["name"].stringValue,
                        "contact_number":json["response"]["data"]["contact_number"].stringValue,
                        "bank_id":json["response"]["data"]["bank_id"].stringValue,
                        "mobile_number":json["response"]["data"]["mobile_number"].stringValue,
                        "created_at":json["response"]["data"]["created_at"].stringValue,
                        "gender":json["response"]["data"]["gender"].stringValue,
                    ]
                    UserDefaults.standard.set(dict, forKey: USER_DEFAULTS_KEYS.IOS_PROFILE_RESPONSE)
//                    UserDefaults.standard.set(true, forKey: USER_DEFAULTS_KEYS.BANK_ID_AUTHORIZED)
                    self.loginWithDLServer()
                }
                else {
                    hideAllProgressOnView(self.view)
                    UIAlertController.showInfoAlertWithTitle("Meddelande", message: json["response"]["status"].stringValue, buttonTitle: "Okej")
                }
            }, errorBlock: { (NSError) in
                hideAllProgressOnView(self.view)
                UIAlertController.showInfoAlertWithTitle("Varna", message: kUnexpectedErrorAlertString, buttonTitle: "Okej")
            })
        }
        else{
            hideAllProgressOnView(self.view)
            UIAlertController.showInfoAlertWithTitle("Varna", message: "Kontrollera internetanslutning", buttonTitle: "Okej")
        }
    }
    
    func loginWithDLServer() {
        if Reachability.isConnectedToNetwork() {
            showProgressOnView(self.view)
            DLServer.sharedInstance.authenticateUser(usernameOrEmail: LOGIN_CREDENTIALS.EMAIL, password: LOGIN_CREDENTIALS.PASSWORD) { (response: DLRequestUserTokenResponse) in
                hideAllProgressOnView(self.view)
                if response.success {
                    AppDelegate.accessToken = response.accessToken
                    print(AppDelegate.accessToken!)  
                    self.navigationController?.pushViewController(AppDelegate.scannedDeviceListViewController(), animated: true)
                }
                else {
                    UIAlertController.showInfoAlertWithTitle("Meddelande", message: "kunde inte logga in. \(response.responseCode)", buttonTitle: "Okej")
                }
            }  
        }
        else{
            hideAllProgressOnView(self.view)
            UIAlertController.showInfoAlertWithTitle("Varna", message: "Kontrollera internetanslutning", buttonTitle: "Okej")
        }
    }
    
    @IBAction func submitBtnTap(_ sender: Any) {
        if bankingIdTextField.text?.isEmpty == true {
            showInvalidInputAlert(bankingIdTextField.placeholder!)
        }
        else {
            let param1:[String:String] = ["bank_id" : bankingIdTextField.text!,
                                          "device_id" : UIDevice.current.identifierForVendor!.uuidString,
                                          "device_type" : "iOS",
                                          "req_ip": getIP()!]
            print(param1)
            if Reachability.isConnectedToNetwork() {
                showProgressOnView(self.view)
                ServerClass.sharedInstance.sendMultipartRequestToServer(urlString: PORT_URL + BANKING_ID_API_URL, sendJson: param1, successBlock: { (json) in
                    hideAllProgressOnView(self.view)
                    let success = json["status"].stringValue
                    if success  == "success" {
                        print(json)
                        UIAlertController.showInfoAlertWithTitle("Varna", message: kUnexpectedErrorAlertString, buttonTitle: "Okej")
                    }
                    else {
                        hideAllProgressOnView(self.view)
                        UIAlertController.showInfoAlertWithTitle("Meddelande", message: json["status"].stringValue, buttonTitle: "Okej")
                    }
                }, errorBlock: { (NSError) in
                    hideAllProgressOnView(self.view)
                    UIAlertController.showInfoAlertWithTitle("Varna", message: kUnexpectedErrorAlertString, buttonTitle: "Okej")
                })
            }
            else{
                hideAllProgressOnView(self.view)
                UIAlertController.showInfoAlertWithTitle("Varna", message: "Kontrollera internetanslutning", buttonTitle: "Okej")
            } 
        }
    }
    
    //let urlStr = "https://app.bankid.com/?autostarttoken=&redirect=norsjoAB://"
    //       let urlStr = "bankid:///?autostarttoken=&redirect=norsjoAB://"
    //
    //        guard let url = URL(string: urlStr) else {
    //            return
    //        }
    //        if UIApplication.shared.canOpenURL(url) {
    ////            UIApplication.shared.open(url, options: [.universalLinksOnly:true]) {
    ////                (success) in
    ////                print(success)
    ////            }
    //            UIApplication.shared.open(URL(string:urlStr)!)
    //        }
    //    }
}
