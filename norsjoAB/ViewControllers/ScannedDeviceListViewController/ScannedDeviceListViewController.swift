//
//  ScannedDeviceListViewController.swift
//  norsjoAB
//
//  Created by Unyscape Infocom on 12/09/19.
//  Copyright © 2019 Unyscape Infocom. All rights reserved.
//

import UIKit

class ScannedDeviceListViewController: UIViewController {
   
    @IBOutlet weak var customNavigationBar: CustomNavigationBar!
    @IBOutlet weak var scannedDeviceListTableView: UITableView!
    
    var scannedDevices: [BluetoothManager.ScannedDevice] = []
    var timer = Timer()

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = #colorLiteral(red: 0.9983497262, green: 0.3577868938, blue: 0.9943968654, alpha: 1)
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationBar.titleLabel.text = "24sju AB"
        customNavigationBar.hideLeftBarButtonItem()
        scannedDeviceListTableView.addSubview(refreshControl)
        scannedDeviceListTableView.backgroundColor = UIColor.white
//        scannedDeviceListTableView.tableFooterView = UIView()
        
//        showProgressOnViewWithMessage(self.view, message: "Scanning...")
//        guard let _ = AppDelegate.accessToken else {
//            UIAlertController.showInfoAlertWithTitle("Fel", message: "kunde inte logga in", buttonTitle: "Okej")
//            return
//        }
//
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(reloadTableView), userInfo: nil, repeats: true)

        BluetoothManager.sharedInstance.startScan { (status) in
//            if status == false {
//                UIAlertController.showInfoAlertWithTitle("Varna", message: "Slå på bluetooth först", buttonTitle: "Okej")
//            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        print(BluetoothManager.sharedInstance.scannedDevices.count)
        scannedDeviceListTableView.reloadData()
        Tools.delay(bySeconds: 1) {
            self.refreshControl.endRefreshing()
        }
    }       
    
    @objc func reloadTableView() {
        scannedDevices = []
        for device in BluetoothManager.sharedInstance.scannedDevices {
            scannedDevices.append(device.value)
            hideProgressOnView(self.view)
        }
        
        scannedDeviceListTableView.reloadData()
        BluetoothManager.sharedInstance.startScan { (status) in
            if status == false {
                hideProgressOnView(self.view)
                self.scannedDevices.removeAll()
                self.scannedDeviceListTableView.reloadData()
                UIAlertController.showInfoAlertWithTitle("Varna", message: "Slå på bluetooth först", buttonTitle: "Okej")
            }
        }
    }
}

extension ScannedDeviceListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scannedDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ScannedDeviceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ScannedDeviceTableViewCell") as! ScannedDeviceTableViewCell
//        cell.deviceNameLbl?.text = "\(scannedDevices[indexPath.row].id), \(scannedDevices[indexPath.row].info!.product.name), owned? \(scannedDevices[indexPath.row].info!.enrolled)"
        cell.deviceNameLbl?.text = scannedDevices[indexPath.row].info!.product.name
        return cell  
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let device = scannedDevices[indexPath.row]
        timer.invalidate()
        self.navigationController?.pushViewController(AppDelegate.lockUnlockViewController(), animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
}
