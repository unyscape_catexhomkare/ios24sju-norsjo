//
//  ScannedDeviceTableViewCell.swift
//  norsjoAB
//
//  Created by Unyscape Infocom on 12/09/19.
//  Copyright © 2019 Unyscape Infocom. All rights reserved.
//

import UIKit

class ScannedDeviceTableViewCell: UITableViewCell {
    @IBOutlet weak var deviceNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
