//
//  ThankYouViewController.swift
//  norsjoAB
//
//  Created by Unyscape Infocom on 13/09/19.
//  Copyright © 2019 Unyscape Infocom. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func goShoppingBtnTap(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: WelcomeScreenViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}              
