//
//  Dmi_AfiClient_Authority.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#ifndef __DMI_AFICLIENT_AUTHORITY_H_
#define __DMI_AFICLIENT_AUTHORITY_H_

#import "Dmi_AfiClient.h"

@interface Dmi_AfiClient_Authority : Dmi_AfiClient
/**
 * Perform a log in to raise privileges.
 */
+ (void) login: (NSData *) loginToken continuation: (Dmi_Basic_Continuation) continuation;

/**
 * Enroll a device. 3 step procedure involving server communication.
 */
+ (void) enroll: (NSData *) enrollmentToken continuation: (Dmi_Array_Continuation) continuation;

/**
 * Enroll a device. 3 step procedure involving server communication.
 */
+ (void) enrollConfirm: (NSData *) enrollmentConfirmToken continuation: (Dmi_Basic_Continuation) continuation;

/**
 * Disenroll a device.
 */
+ (void) disenrollWithContinuation: (Dmi_Array_Continuation) continuation;
@end

#endif
