//
//  Dmi_AfiClient_Device.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#ifndef __DMI_AFICLIENT_DEVICE_H_
#define __DMI_AFICLIENT_DEVICE_H_

#import "Dmi_AfiClient.h"
#import "DMITimeZoneTransition.h"

@interface Dmi_AfiClient_Device : Dmi_AfiClient
/**
 * Request an identification gesture from the device.
 */
+ (void) identifyWithContinuation: (Dmi_Basic_Continuation) continuation;

/**
 * Enter bootloader mode on the device.
 */
+ (void) enableDfuModeWithContinuation: (Dmi_Basic_Continuation) continuation;

/**
 * Get basic device information
 */
+ (void) getDeviceInformation: (Dmi_AfiClient_GetDeviceInformation_Continuation) continuation;

/**
 * Pair device (danapad etc/lock pairing)
 */
+ (void) setPairing: (Dmi_SerialNumber *) deviceId role: (afi_authority_paired_device_identifier) role product: (dmi_product) product continuation: (Dmi_Basic_Continuation) continuation;

/**
 * Get pair information
 */
+ (void) getPairing: (Dmi_AfiClient_GetPairingInformation_Continuation) continuation;

/**
 * Unpair device (danapad etc/lock unpair...)
 */
+ (void) unpairDevice: (Dmi_Basic_Continuation) continuation;

/**
 * Unpair device (danapad etc/lock unpair...)
 */
+ (void) setTimezoneWithOffset: (int32_t) offset transitions: (NSArray<DmiTimeZoneTransition*>*) transitions size: (uint8_t) size continuation: (Dmi_Basic_Continuation) continuation;


@end

#endif
