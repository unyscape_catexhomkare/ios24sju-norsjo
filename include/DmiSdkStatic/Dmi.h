//
//  Dmi.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#ifndef __DMI_H_
#define __DMI_H_

#import <Foundation/Foundation.h>
#import "Dmi_SerialNumber.h"
#import "Dmi_Pin_Code.h"
#import "Dmi_Wifi_AccessPoint.h"


/*
 *  Enum list of danalock devices
 */
typedef enum
{
    DMI_PRODUCT_UNKNOWN,
    DMI_PRODUCT_HTTP_GATEWAY,
    DMI_PRODUCT_EKEY,
    DMI_PRODUCT_DANALOCK,
    DMI_PRODUCT_BRIDGE,
    DMI_PRODUCT_UNIVERSAL,
    DMI_PRODUCT_DANAPAD
} dmi_product;

/*
 *  Struct for holding info of the dmi version. Used in fw update etc.
 */
typedef struct
{
    uint32_t major;
    uint32_t minor;
    uint32_t revision;
} dmi_version;


typedef enum
{
    /*
     * For synchronous functions the operation was completed successfully.
     * For asynchronous functions the operations was started and one or more events will be generated to report on the progress and completion of the operation.
     */
    DMI_STATUS_OK = 0,
    
    /*
     * Represents a general error.
     */
    DMI_STATUS_ERROR = 1,
    
    /*
     * The specified connection is not valid.
     */
    DMI_STATUS_INVALID_CONNECTION = 2,
    
    /*
     * One or more of the parameters passed to the function were out of range. No operation was started and no events will be generated.
     */
    DMI_STATUS_INVALID_PARAMETER = 3,
    
    /*
     * The operation could not be started at the moment. No events will be generated.
     */
    DMI_STATUS_BUSY = 4,
    
    /*
     * The operation could not be started at the moment. An operation specific event will be generated when the operation may be retried.
     */
    DMI_STATUS_BUSY_WITH_NOTIFICATION = 5,
    
    /*
     * The operation could not be performed because the object is not in a required state.
     */
    DMI_STATUS_BAD_STATE = 6,
    
    /*
     * The required amount of memory could not be allocated on the heap.
     */
    DMI_STATUS_HEAP_EXHAUSTED = 7,
    
    /*
     * The operation could not be performed as a required resource has been exhausted.
     */
    DMI_STATUS_OUT_OF_RESOURCES = 8,
    
    /*
     * The requested operation is not implemented.
     */
    DMI_STATUS_NOT_IMPLEMENTED = 9,
    
    /*
     * The requested operation could not be completed as a connection has not been established or a connection could not be established.
     */
    DMI_STATUS_NOT_CONNECTED = 10,
    
    /*
     * The remote node did not reply.
     */
    DMI_STATUS_NO_REPLY = 11,
    
    /*
     * Data has been queued for later processing. Caller may proceed as if DMI_STATUS_OK had been returned.
     */
    DMI_STATUS_QUEUED = 12,
    
    /*
     *
     */
    DMI_STATUS_UNABLE_TO_SEND_PACKET = 13,
    
    /*
     *
     */
    DMI_STATUS_REMOTE_NODE_DISCONNECTED = 14,
    
    /*
     * Connection was lost due to excessive retransmissions or an internal error.
     */
    DMI_STATUS_CONNECTION_LOST = 15,
    
    /*
     * The requested operating did not change anything. E.g. the requested state had already been entered.
     */
    DMI_STATUS_NOTHING_CHANGED = 16,
    
    /*
     *
     */
    DMI_STATUS_NO_ROUTE_TO_DESTINATION = 17,
    
    /*
     * An error occurred in the application protocol. Request more information from the application protocol.
     */
    DMI_STATUS_APPLICATION_LAYER_PROTOCOL_ERROR = 18,
    
    /*
     * An attempt was made to connect while already connected.
     */
    DMI_STATUS_ALREADY_CONNECTED = 19,
    
    /*
     * The specified function is not valid.
     */
    DMI_STATUS_INVALID_FUNCTION = 20,
    
    /*
     * The requested operation or information is not supported.
     */
    DMI_STATUS_NOT_SUPPORTED = 21,
    
    /*
     * The response to a function call was invalid.
     */
    DMI_STATUS_BAD_FUNCTION_RESPONSE = 22,
    
    /*
     * A required parameter was not present.
     */
    DMI_STATUS_MISSING_PARAMETER = 23,
    
    /*
     * More parameters than was expected was present.
     */
    DMI_STATUS_UNEXPECTED_PARAMETER = 24,
    
    /*
     * A timeout occurred.
     */
    DMI_STATUS_TIMEOUT = 25,
    
    /*
     * The handle passed to the function was invalid (e.g. has not been created/opened/initialized or has been closed).
     */
    DMI_STATUS_INVALID_HANDLE = 26,
    
    /*
     * The network interface specified is not valid (e.g. not registered).
     */
    DMI_STATUS_INVALID_NETWORK_INTERFACE = 27,
    
    /*
     * A function call to the platform abstraction layer failed. See log for more information.
     */
    DMI_STATUS_PLATFORM_OPERATION_FAILED = 28,
    
    /*
     * The requested link has broken down and will not automatically be brought up.
     */
    DMI_STATUS_LINK_IS_BROKEN = 29
} dmi_status;

// MARK: Continuations

/**
 * Continuation that only returns a dmi status.
 */
typedef void (^Dmi_Basic_Continuation)(dmi_status status);

/**
 * Continuation that only returns a dmi status and a integer
 */
typedef void (^Dmi_UInt16_Continuation)(dmi_status status, uint16_t integer);

/**
 * Continuation that only returns a dmi status and an array
 */
typedef void (^Dmi_Array_Continuation)(dmi_status status, NSData *array);

/**
 * Continuation that only returns a dmi status and a dictionary
 */
typedef void (^Dmi_AfiClient_GetSettings_Continuation)(dmi_status status, NSDictionary *settings);

/**
 * Continuation used for fetching device info
 */
typedef void (^Dmi_AfiClient_GetDeviceInformation_Continuation)(dmi_status status, dmi_product product, dmi_version hardwareVersion, dmi_version firmwareVersion, NSString *firmwareIdentifier);

/**
 * Continuation used for getting info of device pairing
 */
typedef void (^Dmi_AfiClient_GetPairingInformation_Continuation)(dmi_status status, Dmi_SerialNumber* serialOfPairedDevice);

/**
 * Continuation used for getting pincodes set on a danalock or universal module
 */
typedef void (^Dmi_AfiClient_GetPincodes_Continuation)(dmi_status status, NSArray<Dmi_Pin_Code*> *pincodes);

/**
 * Continuation used for getting access points discovered by your
 */
typedef void (^Dmi_AfiClient_GetAccessPoints_Continuation)(dmi_status status, NSArray<Dmi_Wifi_AccessPoint*> *scanResult);

/**
 * Continuation used for getting time restrictions stored on your danalock or universal module
 */
typedef void (^Dmi_AfiClient_Get_Time_Restrictions_Continuation)(dmi_status status,  NSArray<DmiPinCodeRestrictionRuleWrapper*> *rules);


@interface Dmi : NSObject

/**
 * Initialize the DmiStack. Should only be called once.
 * Must be called before calling anything else, otherwise expect unexpected behaviour.
 */
+ (void) initialize;

/**
 * Sets your local address. This is necessary otherwise the remote device will not know how to respond.
 * @param address Your local address.
 */
+ (void) setLocalAddress: (Dmi_SerialNumber *) address;

/**
 * Callback that will be invoked when the DmiStack needs to send data on BLE.
 * You should transmit the data.
 */
+ (void) setDataGeneratedCallback: (dmi_status (^)(uint8_t id, NSData *data)) callback;

/**
 * Callback whenever the Dmi stacks emits a Debug message.
 */
+ (void) setLogMessageGeneratedCallback: (void (^)(NSString *message)) callback;

/**
 * When you receive data on BLE, pass it to the DmiStack using this method.
 */
+ (void) dataReceived: (NSData *)data;

/**
 * Characteristic which should be watched for notifications.
 */
+ (NSUUID *) readCharacteristicUUID;

/**
 * Characteristic for writing.
 */
+ (NSUUID *) writeCharacteristicUUID;

/**
 * UART GATT service.
 */
+ (NSUUID *) uartServiceUUID;

@end

#endif
