//
//  Dmi_LockV3.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#ifndef __DMI_AFICLIENT_LOCK_H_
#define __DMI_AFICLIENT_LOCK_H_

#import "Dmi_AfiClient.h"
#import "Dmi_Pin_Code.h"

typedef enum
{
    AFI_CLASS_PIN_CODES_SETTINGS_PARAMETER_NONE,
    AFI_CLASS_PIN_CODES_SETTINGS_PARAMETER_STATUS,
    AFI_CLASS_PIN_CODES_SETTINGS_PARAMETER_EASY_LOCKING_STATUS,
} afi_class_pin_codes_settings_parameter;

typedef enum {
    /**
     * Must be between 1-5 including (1 - Slowest - 5 fastest).
     */
    SPEED = 1,	// 1-5 (1 - Slowest - 5 fastest)
    
    /**
     * 0 (disabled), 1 - 255 (seconds)
     */
    AUTO_LATCH = 2, // 0(disabled) - 255 (seconds)
    
    /**
     * 0 (disabled), 1 - 255 (seconds)
     */
    BRAKE_AND_GO_BACK = 3,
    
    /**
     * 0 = Disabled, 1 = Enabled
     */
    TWIST_ASSIST = 4,
    
    /**
     * 0 = Disabled, 1 = Enabled
     */
    BLOCKED_TO_BLOCKED = 5,
} LOCKV3_MECH_SETTINGS;

@interface Dmi_AfiClient_Lock : Dmi_AfiClient

/**
 * Set calibration point in locked position.
 */
+ (void) setCalibrationPoint_LockedPosition: (Dmi_Basic_Continuation) continuation;

/**
 * Set calibration point in unlocked position.
 */
+ (void) setCalibrationPoint_UnlockedPosition: (Dmi_Basic_Continuation) continuation;

/**
 * Perform auto calibration.
 */
+ (void) autoCalibrate: (Dmi_Basic_Continuation) continuation;

/**
 * Perform an unlock.
 */
+ (void) unlock: (Dmi_Basic_Continuation) continuation;

/**
 * Perform a lock.
 */
+ (void) lock: (Dmi_Basic_Continuation) continuation;

/**
 * Put the device in z-wave mode.
 */
+ (void) setZwaveInclusionMode: (Dmi_Basic_Continuation) continuation;

/**
 * Request settings from the lock.
 * @param settings Byte array of settings to query.
 * @note Settings indices are still not finalized.
 */
+ (void) getSettings: (NSArray<NSNumber *>*) settings continuation: (Dmi_AfiClient_GetSettings_Continuation) continuation;

/**
 * Update settings in the lock.
 * @param settings Byte array of settings to change.
 * @note Settings indices are still not finalized.
 */
+ (void) setSettings: (NSDictionary<NSNumber *, NSNumber *>*) settings continuation: (Dmi_Basic_Continuation) continuation;

/**
 * Get danapad settings (Easy lock / vacation mode)
 */
+ (void) getDanapadSettings: (Dmi_AfiClient_GetSettings_Continuation) continuation;

/**
 * Set danapad settings (Easy lock / vacation mode)
 */
+ (void) setDanapadSettings: (NSDictionary*) settings continuation: (Dmi_Basic_Continuation) continuation;

/**
 * get pincodes stored on the lock (for use with danapad)
 */
+ (void) getPincodes: (Dmi_AfiClient_GetPincodes_Continuation) continuation;

/**
 * store pincode on the lock (for use with danapad)
 */
+ (void) setPincode: (NSInteger) pincodeId pinCodeStatus: (afi_class_pin_codes_pin_code_status) pinCodeStatus digits: (NSArray<NSNumber*>*) pinDigits continuation: (Dmi_Basic_Continuation) continuation;

/**
 * get pincodes stored on the lock (for use with danapad)
 */
+ (void) getPincodeTimeRestrictions: (NSInteger) pincodeId continuation: (Dmi_AfiClient_Get_Time_Restrictions_Continuation) continuation;

/**
 * sets a user specefic time restriction for a pin code
 */
+ (void) setPincodeTimeRestriction: (NSInteger) pincodeId rule:(afi_class_pin_codes_time_restriction_rule) rule continuation: (Dmi_UInt16_Continuation) continuation;

/**
 * store pincode on the lock (for use with danapad)
 */
+ (void) deletePincodeTimeRestrictions: (NSInteger) handle continuation: (Dmi_Basic_Continuation) continuation;

@end

#endif
