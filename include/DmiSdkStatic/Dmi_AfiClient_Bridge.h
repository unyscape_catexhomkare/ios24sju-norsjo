//
//  Dmi_Bridge.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#ifndef __DMI_AFICLIENT_BRIDGE_H_
#define __DMI_AFICLIENT_BRIDGE_H_

#import "Dmi_AfiClient.h"

@interface Dmi_AfiClient_Bridge : Dmi_AfiClient

/**
 * Connect the bridge to wifi.
 */
+ (void) wifiConnect: (NSString *) ssid passphrase: (NSString *) passphrase continuation: (Dmi_Basic_Continuation) continuation;
+ (void) wifiDisconnect: (Dmi_Basic_Continuation) continuation;

//+ (void) getConnectionState: (Dmi_Bridge_Get_State_Continuation) continuation;

+ (void) wifiScan: (Dmi_AfiClient_GetAccessPoints_Continuation) continuation;
+ (void) getCurrentAccessPoint: (Dmi_AfiClient_GetAccessPoints_Continuation) continuation;

@end

#endif
