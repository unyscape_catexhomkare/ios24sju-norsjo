//
//  DMITimeZoneTransition.h
//  DmiSdkStatic
//
//  Created by Tomas Clausen on 17/04/2019.
//  Copyright © 2019 Daniel Amkær Sørensen. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DmiTimeZoneTransition : NSObject

@property (nonatomic, readonly) uint32_t startTime;
@property (nonatomic, readonly) uint32_t offset;

- (id) initWithStarttime: (uint32_t) starttime offset: (uint32_t) offset;

@end
