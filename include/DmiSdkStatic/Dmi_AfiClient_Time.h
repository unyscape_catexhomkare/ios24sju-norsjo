//
//  Dmi_AfiClient_Time.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#ifndef __DMI_AFICLIENT_TIME_H_
#define __DMI_AFICLIENT_TIME_H_

#import "Dmi_AfiClient.h"

@interface Dmi_AfiClient_Time : Dmi_AfiClient
/**
 * Update the UTC time in the device. The time will be taken from the internal clock.
 */
+ (void) setUtcTime: (Dmi_Basic_Continuation) continuation;
@end

#endif
