//
//  DmiSdkStatic.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#include "Dmi.h"
#include "Dmi_SerialNumber.h"
#include "Dmi_AfiClient.h"
#include "Dmi_AfiClient_Device.h"
#include "Dmi_AfiClient_Time.h"
#include "Dmi_AfiClient_Authority.h"
#include "Dmi_AfiClient_Universal_Module.h"
#include "Dmi_AfiClient_Lock.h"
#include "Dmi_AfiClient_Bridge.h"
#include "Dmi_BroadcastReceiver.h"
#include "Dmi_Pin_Code.h"
#include "DMITimeZoneTransition.h"
