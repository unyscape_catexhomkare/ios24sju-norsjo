//
//  Dmi_BroadcastReceiver.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "Dmi_SerialNumber.h"

@interface Dmi_BroadcastContent_Generic : NSObject

/**
 * Counter that will increase, in order to always have different ciphertext.
 */
@property (nonatomic, readonly) uint16_t updateCounter;

/**
 * Indicates whether the time is correctly set.
 */
@property (nonatomic, readonly) BOOL utcIsReliable;

/**
 * Indicates whether the local time source is running.
 */
@property (nonatomic, readonly) BOOL localtimeIsReliable;

/**
 * Indicates that the device is battery powered.
 */
@property (nonatomic, readonly) BOOL batteryPowered;

/**
 * Indicates that the device has an extension board (Zigbee, Z-wave etc.)
 */
@property (nonatomic, readonly) BOOL extensionBoardPresent;

/**
 * Indicates that the extension board is online on its network.
 */
@property (nonatomic, readonly) BOOL extensionBoardOnline;

/**
 * Indicates that a bridge is in proximity of the device.
 */
@property (nonatomic, readonly) BOOL bridgeInProximity;

/**
 * Indicates that a bridge is paired.
 */
@property (nonatomic, readonly) BOOL bridgePaired;

/**
 * Indicates that there are new log items that have not been delivered.
 */
@property (nonatomic, readonly) BOOL logItemsAvailable;

/**
 * Indicates the power source level. nil if unknown.
 */
@property (nonatomic, readonly) NSNumber *powerSourceLevel;

/**
 * For future diagnostics use.
 */
@property (nonatomic, readonly) NSNumber *diagnosticsChecksum;

/**
 * Indicates that the lock is latched.
 */
@property (nonatomic, readonly) BOOL lockIsLocked;

@end


@interface Dmi_BroadcastContent_UniversalModuleV3 : Dmi_BroadcastContent_Generic

/**
 * Indicates that the relay is in timed mode.
 */
@property (nonatomic, readonly) BOOL isRelayModeTimed;

@end


@interface Dmi_BroadcastContent_LockV3 : Dmi_BroadcastContent_Generic


/**
 * Indicates that the lock has performed auto calibration.
 */
@property (nonatomic, readonly) BOOL lockIsAutoCalibrated;

/**
 * Indicates that the lock has been point calibrated.
 */
@property (nonatomic, readonly) BOOL lockIsPointCalibrated;

/**
 * Indicates that the lock has been blocked. Position may not be reliable.
 */
@property (nonatomic, readonly) BOOL lockIsBlocked;

/**
 * Indicates that brake and go back is enabled.
 */
@property (nonatomic, readonly) BOOL lockIsBrakeAndGoBackEnabled;

/**
 * Indicates that auto lock is enabled.
 */
@property (nonatomic, readonly) BOOL lockIsAutolockEnabled;

/**
 * Indicates that the lock always run from end-to-end.
 */
@property (nonatomic, readonly) BOOL lockIsBlockedToBlockedEnabled;

/**
 * Indicates that the lock has twist assist enabled.
 */
@property (nonatomic, readonly) BOOL lockIsTwistAssistEnabled;

@end

@interface Dmi_BroadcastContent_Bridge : Dmi_BroadcastContent_Generic

@end

@interface Dmi_BroadcastContent : NSObject

/**
 * Whether the Broadcast is encrypted.
 * If it is encrypted, decodeAs* methods will fail.
 */
@property (readonly) BOOL isEncrypted;

/**
 * Indicates whether the device is enrolled.
 */
@property (readonly) BOOL isEnrolled;

/**
 * Get the serial number from the broadcast.
 */
@property (nonatomic, readonly) Dmi_SerialNumber *serialNumber;

/**
 * Try to decrypt using the given key.
 * @return Dmi_BroadcastContent new instance of the class with the data decrypted. nil if decryption failed.
 */
- (Dmi_BroadcastContent *) decryptWithKey: (NSData *) key;

/**
 * Try to decode the flags as a generic device.
 */
- (Dmi_BroadcastContent_Generic *) decodeAsGeneric;

/**
 * Try to decode the flags as a lock v3.
 */
- (Dmi_BroadcastContent_LockV3 *) decodeAsLockV3;

/**
 * Try to decode the flags as a v3 universal module.
 */
- (Dmi_BroadcastContent_UniversalModuleV3 *) decodeAsUniversalModuleV3;

/**
 * Try to decode the flags as a bridge.
 */
- (Dmi_BroadcastContent_Bridge *) decodeAsBridge;

@end

@interface Dmi_BroadcastReceiver : NSObject
/**
 * Decode an advertisement.
 * @param manufacturerData Comes from CB discovery event. 
 * @see CBAdvertisementDataManufacturerDataKey
 */
+ (Dmi_BroadcastContent *) createFromAdvertisement: (NSData *) manufacturerData;
@end
