//
//  Dmi_Afi.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#ifndef __DMI_AFICLIENT_H_
#define __DMI_AFICLIENT_H_

#include "Dmi.h"

/*
 * Paired device tag.
 */
typedef enum
{
    AFI_AUTHORITY_PAIRED_DEVICE_TAG_NONE = 0,
    /*
     *    Dmi Serial Number
     */
    AFI_AUTHORITY_PAIRED_DEVICE_TAG_SERIAL_NUMBER = 1,
    /*
     * Product Identifier.
     */
    AFI_AUTHORITY_PAIRED_DEVICE_TAG_PRODUCT_IDENTFIER = 2
} afi_authority_paired_device_tag;

/*
 * Paired device identifier.
 * This identifies what kind of device that has been paired.
 */
typedef enum
{
    AFI_AUTHORITY_PAIRED_DEVICE_IDENTIFIER_UNKNOWN = 0,
    
    /*
     *    The DanaPad must have paired a DanaPad server.
     *    The DanaPad then knows what server to connect to when
     *    a pin code needs validation and a operations must be handled.
     */
    AFI_AUTHORITY_PAIRED_DEVICE_IDENTIFIER_DANAPAD_SERVER = 1,
    
    /*
     * If a DanaServer should allow a DanaPad to use UserCode login.
     * It must have paired this device.
     */
    AFI_AUTHORITY_PAIRED_DEVICE_IDENTIFIER_DANAPAD_CLIENT = 2,
    
    /*
     * To pair with bridge.
     * (furture purpose.)
     */
    AFI_AUTHORITY_PAIRED_DEVICE_IDENTIFIER_BRIDGE = 3
} afi_authority_paired_device_identifier;

/*
 * Afi lock state
 * This describes what state the danalock is currently in
 */
typedef enum
{
    AFI_LOCK_MECHANISM_STATE_UNKNOWN,
    AFI_LOCK_MECHANISM_STATE_UNLOCKED,
    AFI_LOCK_MECHANISM_STATE_LOCKED,
} afi_lock_mechanism_state;

/*
 * Afi lock state
 * This describes what state the danalock is currently in
 */
typedef enum
{
    AFI_LOCK_MECHANISM_CALIBRATION_POINT_LOCKED = 1,
    AFI_LOCK_MECHANISM_CALIBRATION_POINT_UNLOCKED = 0,
} afi_lock_mechanism_calibration_point;

/*
 * The state of the auto calibration procedure.
 */
typedef enum
{
    AFI_LOCK_MECHANISM_AUTO_CALIBRATION_PROCEDURE_STATE_IDLE,
    AFI_LOCK_MECHANISM_AUTO_CALIBRATION_PROCEDURE_STATE_INITIATED,
    AFI_LOCK_MECHANISM_AUTO_CALIBRATION_PROCEDURE_STATE_COMPLETED
} afi_lock_mechanism_auto_calibration_procedure_state;


typedef struct
{
    /*
     * The reason for the disconnect.
     */
    dmi_status status;
} afi_client_notification_data_disconnected;

/*
 * The type passed to the event handler when the AFI_CLIENT_EVENT_IDENTIFIER_NOTIFICATION_LOCK_STATE_UPDATED notification is received.
 */
typedef struct
{
    /*
     * The current state of the lock.
     */
    afi_lock_mechanism_state state;
} afi_client_notification_data_lock_state_updated;

/*
 * The type passed to the event handler when the 	AFI_CLIENT_EVENT_IDENTIFIER_NOTIFICATION_AUTO_CALIBRATION_STATE_UPDATED notification is received.
 */
typedef struct
{
    /*
     * The current state of the auto calibration procedure.
     */
    afi_lock_mechanism_auto_calibration_procedure_state state;
} afi_client_notification_data_auto_calibration_state_updated;

/******************************************************************************
 *
 * API management and events
 *
 *****************************************************************************/

/*
 * List of all possible event identifiers.
 */
typedef enum
{
    /* Errors */
    AFI_CLIENT_EVENT_IDENTIFIER_ERROR,
    
    /* Notifications */
    AFI_CLIENT_EVENT_IDENTIFIER_NOTIFICATION_DISCONNECTED,
    AFI_CLIENT_EVENT_IDENTIFIER_NOTIFICATION_LOCK_STATE_UPDATED,
    AFI_CLIENT_EVENT_IDENTIFIER_NOTIFICATION_AUTO_CALIBRATION_STATE_UPDATED,
} afi_client_event_identifier;

/*
 * The event object passed in call to afi_client_event. The appropriate union member must be selected based on the identifier given in the event object..
 */
typedef struct
{
    afi_client_event_identifier identifier;
    
    union
    {
        /* Errors */
        dmi_status error;
        
        /* Notifications */
        afi_client_notification_data_disconnected notificationDisconnected;
        afi_client_notification_data_lock_state_updated notificationLockStateUpdate;
        afi_client_notification_data_auto_calibration_state_updated notificaitonAutoCalibrationStateUpdate;
    } data;
} afi_client_event;

typedef enum
{
    AFI_STATUS_OK = 0,
    
    AFI_STATUS_ERROR = 1,
    
    AFI_STATUS_UNSUPPORTED_OPCODE = 2,
    AFI_STATUS_UNSUPPORTED_CLASS = 3,
    AFI_STATUS_UNSUPPORTED_COMMAND = 4,
    AFI_STATUS_METHOD_BUSY = 5,
    AFI_STATUS_ARGUMENT_ERROR = 6,
    AFI_STATUS_NOTHING_CHANGED = 7,
    AFI_STATUS_NOT_READY = 8,
    AFI_STATUS_UNSUPPORTED_ARGUMENT = 9,
    
    AFI_STATUS_NOT_AVAILABLE = 10,
    
    AFI_STATUS_NOTHING_TO_DO = 16,
    
    AFI_STATUS_AUTHORIZATION_ERROR_NOT_PERMITTED = 64,
    AFI_STATUS_AUTHORIZATION_ERROR_NOT_LOGGED_IN = 65,
    
    AFI_STATUS_AUTHORIZATION_ERROR_TOKEN_VERSION_NOT_SUPPORTED = 74,
    AFI_STATUS_AUTHORIZATION_ERROR_TOKEN_FORMAT_ERROR = 75,
    
    AFI_STATUS_AUTHORIZATION_ERROR_TBS_FORMAT_ERROR = 84,
    AFI_STATUS_AUTHORIZATION_ERROR_TBS_MISSING_ERROR = 85,
    AFI_STATUS_AUTHORIZATION_ERROR_TBS_SERIAL_NUMBER_MISSING_ERROR = 86,
    AFI_STATUS_AUTHORIZATION_ERROR_TBS_SERIAL_NUMBER_INVALID_ERROR = 87,
    AFI_STATUS_AUTHORIZATION_ERROR_TBS_AFI_PUBLIC_KEY_MISSING_ERROR = 88,
    AFI_STATUS_AUTHORIZATION_ERROR_TBS_CERTIFICATE_MISSING_ERROR = 89,
    AFI_STATUS_AUTHORIZATION_ERROR_TBS_INVALID_TYPE_ERROR = 90,
    
    AFI_STATUS_AUTHORIZATION_ERROR_TIME_VALID_FROM_FAILED = 94,
    AFI_STATUS_AUTHORIZATION_ERROR_TIME_VALID_TO_FAILED = 95,
    AFI_STATUS_AUTHORIZATION_ERROR_TIME_VALID_FROM_MISSING = 96,
    AFI_STATUS_AUTHORIZATION_ERROR_TIME_VALID_TO_MISSING = 97,
    
    AFI_STATUS_AUTHORIZATION_ERROR_SIGNATURE_MISSING_ERROR = 104,
    AFI_STATUS_AUTHORIZATION_ERROR_SIGNATURE_INVALID_ERROR = 105,
    AFI_STATUS_AUTHORIZATION_ERROR_CERTIFICATE_MISSING_ERROR = 106,
    AFI_STATUS_AUTHORIZATION_ERROR_CERTIFICATE_INVALID_ERROR = 107,
    AFI_STATUS_AUTHORIZATION_ERROR_ILLEGAL_SERVER_DOMAIN_ERROR = 108,
    AFI_STATUS_AUTHORIZATION_ERROR_SERVER_DOMAIN_SIZE_ERROR = 109,
    AFI_STATUS_AUTHORIZATION_ERROR_ILLEGAL_ORGANIZATIONAL_UNIT_ERROR = 110,
    
    AFI_STATUS_AUTHORIZATION_ERROR_HSM_FAILED = 114,
    AFI_STATUS_AUTHORIZATION_ERROR_HSM_SIGNING_FAILED = 115,
    
    AFI_STATUS_AUTHORIZATION_ERROR_PIN_CODE_INVALID = 130,
    AFI_STATUS_AUTHORIZATION_ERROR_DEVICE_IS_NOT_PAIRED = 131,
    AFI_STATUS_AUTHORIZATION_ERROR_ILLEGAL_PRODUCT = 132,
    AFI_STATUS_AUTHORIZATION_ERROR_ILLEGAL_DEVICE = 133,
    AFI_STATUS_AUTHORIZATION_ERROR_PIN_CODES_DISABLED = 134,
    AFI_STATUS_AUTHORIZATION_ERROR_PIN_CODES_TIMEOUT = 135,
} afi_status;
//@interface Dmi_AfiClient_Event : NSObject
//- (afi_client_event_identifier) identifier;
//- (afi_lock_mechanism_state) lockMechanishmState;
//- (afi_lock_mechanism_auto_calibration_procedure_state) autoCalibrationProcedureState;
//- (dmi_status) disconnectReason;
//- (dmi_status) error;
//@end

//typedef void (^Dmi_AfiClient_EventHandler)(Dmi_AfiClient_Event *event);

@interface Dmi_AfiClient : NSObject
/**
 * Set an event handler that will receive events, such as errors, notifications etc.
 * Refer to other documentation in this case.
 */
//+ (void) setEventHandler: (Dmi_AfiClient_EventHandler) eventHandler;

/**
 * Connect to a device using serial number.
 * Underlying BLE connection must be established before calling this.
 */
+ (dmi_status) connect: (Dmi_SerialNumber *)address continuation: (Dmi_Basic_Continuation) continuation;

/**
 * Disconnect from the device.
 */
+ (dmi_status) disconnectWithContinuation: (Dmi_Basic_Continuation) continuation;

/**
 * Get the last error received from Afi.
 */
+ (afi_status) lastError;


+ (dmi_status) phoneBleConnectionClosed;


@end


#endif
