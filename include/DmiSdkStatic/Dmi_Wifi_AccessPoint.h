//
//  scan.h
//  DmiSdkStatic
//
//  Created by Tomas Clausen on 12/11/2018.
//  Copyright © 2018 Daniel Amkær Sørensen. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    AFI_CLASS_BRIDGE_ACCESS_POINT_ENCRYPTION_UNKNOWN = 0,
    AFI_CLASS_BRIDGE_ACCESS_POINT_ENCRYPTION_OPEN = 1,
    AFI_CLASS_BRIDGE_ACCESS_POINT_ENCRYPTION_WEP = 2,
    AFI_CLASS_BRIDGE_ACCESS_POINT_ENCRYPTION_WPA_PSK = 3,
    AFI_CLASS_BRIDGE_ACCESS_POINT_ENCRYPTION_WPA2_PSK = 4,
    AFI_CLASS_BRIDGE_ACCESS_POINT_ENCRYPTION_WPA_WPA2_PSK = 5,
    
    /**
     * (NOT SUPPORTED by WT8266 can NOT connect to WPA2_Enterprise AP)
     */
    AFI_CLASS_BRIDGE_ACCESS_POINT_ENCRYPTION_WPA2_ENTERPRISE = 6
} afi_class_bridge_access_point_encryption;

@interface Dmi_Wifi_AccessPoint : NSObject

@property (nonatomic, readonly) NSString * ssid;
@property (nonatomic, readonly) int rssi;
@property (nonatomic, readonly) afi_class_bridge_access_point_encryption encryption;

@end
