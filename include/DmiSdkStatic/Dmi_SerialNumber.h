//
//  Dmi_SerialNumber.h
//  DmiSdkStatic
//
//  Created by Poly-Control ApS on 23/10/2016.
//  Copyright © 2016 Poly-Control ApS. All rights reserved.
//

#ifndef __DMI_SERIALNUMBER_H_
#define __DMI_SERIALNUMBER_H_

#import <Foundation/Foundation.h>

@interface Dmi_SerialNumber : NSObject

/**
 * Initialize a serial number from raw bytes.
 */
- (id) init: (UInt8[6])address;

/**
 * Initialize a serial number from a string, formatted as XX:XX:XX:XX:XX:XX, where X is a hex digit.
 * @return Dmi_SerialNumber if valid string, nil if unable to parse. 
 */
- (id) initWithColonSeparatedString: (NSString *) string;

/**
 * Returns a string formatted as XX:XX:XX:XX:XX:XX.
 */
- (NSString *) colonSeparatedString;

@end

#endif
