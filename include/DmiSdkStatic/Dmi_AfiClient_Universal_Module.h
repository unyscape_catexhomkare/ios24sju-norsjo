//
//  Dmi_AfiClient_Universal_Module.h
//  DmiSdkStatic
//
//  Created by Tomas Clausen on 31/08/2017.
//  Copyright © 2017 Daniel Amkær Sørensen. All rights reserved.
//

#ifndef Dmi_AfiClient_Universal_Module_h
#define Dmi_AfiClient_Universal_Module_h

#import "Dmi_AfiClient.h"

typedef enum
{
    AFI_CLASS_OUTPUTS_OUTPUT_ID_NONE = 0,
    AFI_CLASS_OUTPUTS_OUTPUT_ID_1 = 1,	// Potential Free Relay 1
    AFI_CLASS_OUTPUTS_OUTPUT_ID_2 = 2,  // Potential Free Relay 2
    AFI_CLASS_OUTPUTS_OUTPUT_ID_3 = 3,	// VOUT
} UNIVERSAL_MODULE_SETTINGS;

typedef enum
{
    AFI_CLASS_OUTPUTS_OUTPUT_SETTING_ID_NONE = 0,
    AFI_CLASS_OUTPUTS_OUTPUT_SETTING_ID_PULSE_DELAY = 1,
    AFI_CLASS_OUTPUTS_OUTPUT_SETTING_ID_PULSE_TIME = 2,
} afi_class_outputs_output_setting_id;


@interface Dmi_AfiClient_Universal_Module : NSObject

+ (void) getSettingsForRelay1and2: (Dmi_AfiClient_GetSettings_Continuation) continuation;
+ (void) setSettings: (NSDictionary*) settings continuation: (Dmi_Basic_Continuation) continuation;

@end

#endif /* Dmi_AfiClient_Universal_Module_h */
